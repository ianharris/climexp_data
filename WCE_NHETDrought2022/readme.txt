data for attribution analysis European drought 2022, T, pr and soil moisture 
E-OBS via Climate Explorer
ERA5 T and pr via Mariam Zachariam m.zachariah@imperial.ac.uk via Climexp (t2m_NHET obtained on 1-11-2022)
ERA5 swvl via Dominik Schumacher; swvl1 corresponds to 0-7cm, and swvl1234 is down to a depth of 2.89m and is hence the closest match to the CMIP6 variable 'mrso' (column soil moisture). swvl123 is down to 1m.
CMIP6 via Dominik Schumacher  dominik.schumacher@env.ethz.ch
FLOR, AM2.5 via Wenchang Yang wenchang@princeton.edu
Highresmip via Mariam Zachariam m.zachariah@imperial.ac.uk (no rootzone moisture)