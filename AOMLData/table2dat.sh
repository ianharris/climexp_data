#!/bin/bash
echo "copy table from https://www.aoml.noaa.gov/hrd-faq/#tcs-after-1930"
for type in ns hu ih ace; do
    case $type in
        ns) col=2; name="number of Atlantic names storms";;
        hu) col=3; name="number of Atlantic hurricanes";;
        ih) col=4; name="number of Atlantic major hurricanes";;
        ace) col=5; name="Atlantic Accumulated Cyclone Energy";;
        *) echo "errror mnbvcgfew";exit -1;;
    esac
    cat > landsea_$type.dat <<EOF
# $type [1] $name
# from https://www.aoml.noaa.gov/hrd-faq/#tcs-after-1930
# institution :: AOML
# source :: https://www.aoml.noaa.gov/hrd-faq/#tcs-after-1930
# generated `date`
EOF
    egrep -e '^1|^2' table.html | awk "{print \$1 \" \" \$$col}" >> landsea_$type.dat
done
