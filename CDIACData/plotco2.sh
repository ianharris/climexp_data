#!/bin/sh
# make a few nice plots

month=`date +%m`
month=${month#0} # get rid of possible leading zero
if [ $month -ge 7 ]; then
    yeardef="-1"
else
    yeardef="1"
fi
daily2longer co2_reallymonthly.dat $yeardef mean > co2_annual.dat
diffdat maunaloa_f_mean1.dat 2 > diff_maunaloa_mean1.dat
diffdat co2_annual.dat 2 > diff_co2_annual.dat
for scen in RCP3PD RCP45 RCP6 RCP85
do
    [ ! -s diff${scen}_CO2.dat ] && diffdat ${scen}_CO2.dat 2 > diff_${scen}_CO2.dat
done
gnuplot plotco2.gnuplot
gnuplot plotdco2.gnuplot
