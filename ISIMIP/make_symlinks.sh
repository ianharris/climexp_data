#!/bin/bash
var=potevap
cd $var
iens=0
ens=000
for gcm in GFDL HadGEM IPSL MIROC5; do
    for hydro in H08 LPJML PCR-globwb watergap; do
        file=${gcm}_${hydro}_${var}.nc
        link=isimip_${var}_rcp60_${ens}.nc
        [ ! -s $file ] && echo "$0: error: cannot find $file" && exit -1
        [ -L $link ] && rm $link
        ln -s $file $link
        ((iens++))
        ens=`printf %03i $iens`
    done
done
