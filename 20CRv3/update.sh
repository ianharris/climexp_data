#!/bin/bash

# daily mean data

base=ftp://ftp.cdc.noaa.gov/Datasets/20thC_ReanV3/Dailies
yrbegin=1836
yrend=2015

for var in prate # uwnd.10m vwnd.10m # hgt tmax.2m tmin.2m air.2m prate prmsl air
do
    if [ ! -s ${var}_daily.nc -a ! -s ${var}850_daily.nc -a ! -s ${var}500_daily.nc ]; then
        case $var in
            air.2m|tmin.2m|tmax.2m) dir=2m;;
            prate) dir=sfc;;
            ?wnd.10m) dir=10m;;
            prmsl) dir=misc;;
            air|hgt) dir=prs;;
            *) echo "$0: error: unknown var $var";exit -1;;
        esac
        case $var in
            air) lev=850;;
            hgt) lev=500;;
        esac
        yr=$yrbegin
        while [ $yr -le $yrend ]; do
            file=$var.$yr.nc
            if [ $yr -le 1980 ]; then
                ddir=${dir}SI
            else
                ddir=${dir}MO
            fi
            echo "updating $ddir/$file"
            if [ $dir = prs ]; then
                outfile=${var}${lev}.$yr.nc
                if [ ! -s $outfile ]; then
                    set -x
                    ncks -v $var -d level,${lev}.  https://www.esrl.noaa.gov/psd/thredds/dodsC/Datasets/20thC_ReanV3/Dailies/$ddir/$file $outfile
                    set +x
                    ###describefield $outfile
                fi
            else
                wget -N $base/$ddir/$file
            fi
            if [ -n "$donotuseopendap" ]; then
                if [ $dir = prs ]; then
                    if [ $var = air ]; then
                        for lev in 850; do
                            if [ ! -s ${var}${lev}.$yr.nc ]; then
                                cdo sellevel,$lev ${var}.$yr.nc ${var}${lev}.$yr.nc
                            fi
                        done
                    elif [ $var = hgt ]; then
                        for lev in 500; do
                            if [ ! -s ${var}${lev}.$yr.nc ]; then
                                cdo sellevel,$lev ${var}.$yr.nc ${var}${lev}.$yr.nc
                            fi
                        done
                    fi
                fi
            fi # commented out
            ((yr++))
        done
        if [ $dir = prs ]; then
            if [ $var = air ]; then
                for lev in 850; do
                    cdo -b 32 -r -f nc4 -z zip copy $var$lev.????.nc ${var}${lev}_daily.nc
                    describefield ${var}${lev}_daily.nc
                    $HOME/NINO/copyfiles.sh ${var}${lev}_daily.nc
                done
            elif [ $var = hgt ]; then
                for lev in 500; do
                    cdo -b 32 -r -f nc4 -z zip copy $var$lev.????.nc ${var}${lev}_daily.nc
                    describefield ${var}${lev}_daily.nc
                    $HOME/NINO/copyfiles.sh ${var}${lev}_daily.nc
                done
            fi
        else
            cdo -b 32 -r -f nc4 -z zip copy $var.????.nc ${var}_daily.nc
            describefield ${var}_daily.nc
            $HOME/NINO/copyfiles.sh ${var}_daily.nc
        fi
    fi
done

# monthly mean data

base=ftp://ftp.cdc.noaa.gov/Datasets/20thC_ReanV3/Monthlies

wget -N ftp://ftp.cdc.noaa.gov/Datasets/20thC_ReanV3/timeInvariantMO/land.nc
wget -N ftp://ftp.cdc.noaa.gov/Datasets/20thC_ReanV3/timeInvariantMO/hgt.sfc.nc
$HOME/NINO/copyfiles.sh land.nc hgt.sfc.nc

for var in prmsl air.2m skt tmax.2m tmin.2m prate rhum.2m vwnd.10m uwnd.10m icec snod soilw shtfl lhtfl dswrf.sfc ulwrf.sfc ulwrf.ntat shum.2m air vwnd uwnd hgt shum 
do
  case $var in
	  air|?wnd|hgt|?hum) dir=prsSI-MO;;
	  prmsl) dir=miscSI-MO;;
	  *.2m) dir=2mSI-MO;;
	  prate|icec|?htfl|dswrf.sfc|ulwrf.sfc|skt) dir=sfcSI-MO;;
	  ulwrf.ntat) dir=ntatFlxSI-MO;;
	  ?wnd.10m) dir=10mSI-MO;;
	  snod) dir=accumsSI-MO;;
	  soilw) dir=subsfcSI-MO;;
	  *) echo "$0: unknown variable $var, check directory";exit -1;;
  esac

  file=$var.mon.mean.nc
  [ ! -s $file -o "$1" = force ] && wget -N $base/$dir/$file

  if [ ! -s $file ]; then
    echo "$0: something went wrong in downloading $base/$dir/$file"
    exit -1
  fi

  if [ $dir = prsSI-MO ]; then
	  for level in 200 300 500 700 850
	  do
		if [ $var.mon.mean.nc -nt $var$level.nc -o "$1" = force ]; then
			ncks -O -d level,$level. $var.mon.mean.nc $var$level.nc
		fi
		$HOME/NINO/copyfiles.sh $var$level.nc
	  done
  else
	  $HOME/NINO/copyfiles.sh $file
  fi
done
./make_snao.sh

cdo -b 32 -f nc4 -z zip divc,2260000 lhtfl.mon.mean.nc evap.mon.mean.nc
ncrename -v lhtfl,evap evap.mon.mean.nc
# this file still has a dimension "x" that confuses my routines
# ncrename -d x,nbnds evap.mon.mean.nc  bug in ncrename, gives invalid netcdf file
ncks -v evap evap.mon.mean.nc tmpevap.mon.mean.nc
mv tmpevap.mon.mean.nc evap.mon.mean.nc
ncatted -a units,evap,m,c,"kg m-2 s-1" -a var_desc,evap,d,c,"" \
    -a standard_name,evap,m,c,"Evapotranspiration" \
    -a long_name,evap,m,c,"evaporation estimated from latent heat flux" evap.mon.mean.nc
ncks -v prate prate.mon.mean.nc tmpprate.mon.mean.nc
cdo -b 32 -f nc4 -z zip sub tmpprate.mon.mean.nc evap.mon.mean.nc pme.mon.mean.nc
rm tmpprate.mon.mean.nc
ncrename -v prate,pme pme.mon.mean.nc
ncatted -a standard_name,pme,d,c,"" \
    -a long_name,pme,m,c,"precipitation minus evaporation" pme.mon.mean.nc
$HOME/NINO/copyfiles.sh evap.mon.mean.nc pme.mon.mean.nc
