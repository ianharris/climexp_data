#!/bin/sh
if [ ! -s snao_20c.nc ]; then
	eof prmsl.mon.mean.nc 1 mon 7 ave 2 begin 1950 end 2010 lon1 -90 lon2 30 lat1 40 lat2 70 snao_20c.nc
fi
patternfield prmsl.mon.mean.nc snao_20c.nc eof1 7 > snao_raw.dat
cat <<EOF > snao_20c.dat
# Summer NAO index (PC of first EOF of SLP over 40-70N, 90W-30E July-August average over 1950-2010)
# based on the 20CRv3 reanalysis sea-level pressure reconstruction
# SNAO [1] 20C Summer NAO
EOF
fgrep ' :: ' snao_raw.dat >> snao_20c.dat
normdiff snao_raw.dat nothing mon mon | fgrep -v '#' >> snao_20c.dat
$HOME/NINO/copyfilesall.sh snao_20c.dat
