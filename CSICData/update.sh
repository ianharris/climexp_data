#!/bin/sh
# SPEI drought indices fields from CSIC (http://sac.csic.es/spei/index.html)
###base=http://digital.csic.es/bitstream/10261/48169
###base=https://digital.csic.es/bitstream/10261/104742
base=https://spei.csic.es/spei_database/nc/
for month in 01 03 04 06 08 12 16 24 36 48
do
	file=spei$month.nc
	wget --no-check-certificate -N $base/$dir/$file
	describefield $file
done
$HOME/NINO/copyfiles.sh spei??.nc
