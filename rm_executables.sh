#!/bin/bash
# to be run after an OS upgrade
for file in */*.f90; do
    f=${file%.f90}
    if [ -x $f ]; then
	    rm $f
    fi
done
if [ -z "$PVM_ARCH" ]; then
    echo "$0: pleaes set PVM_ARCH"
    exit -1
fi
for file in Fortran/$PVM_ARCH/*; do
    if [ -x $file ]; then
	    rm $file
    fi
done
