#!/bin/sh
dest=$HOME/www2/klimatologie/maandoverzicht_wereldweer/
mo=`date -d "1 month ago" "+%b" | tr "[:upper:]" "[:lower:]"`
yr=`date -d "1 month ago" "+%Y"`
echo cp -p *_$mo$yr.png *_$mo$yr.txt $dest
echo cp -p i*.png *_$yr$mo.txt $dest
