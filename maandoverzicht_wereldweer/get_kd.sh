#!/bin/sh
if [ -z "$1" ]; then
  echo "usage: $0 yyyymm"
  exit -1
fi
mv tg_dag260.png tg_dag260_old.png
wget http://www2.knmi.nl/klimatologie/grafieken/jaar/tg_dag260.png
pngtopnm tg_dag260.png | pnmscale 0.6666666667 | pnmtopng > tg_dag260_s.png
pngtopnm tg_dag260.png | pnmtops > tg_dag260.eps
[ -f rdsom$1.png ] && mv rdsom$1.png rdsom$1_old.png
wget http://www2.knmi.nl/klimatologie/geografische_overzichten/maand/archief/mnd-rd/rdsom$1.png
pngtopnm rdsom$1.png | pnmscale 0.6666666667 | pnmtopng > rdsom$1_s.png
pngtopnm rdsom$1.png | pnmtops > rdsom$1.eps
