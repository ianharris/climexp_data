#!/bin/sh
wget -N http://www.cpc.ncep.noaa.gov/products/precip/CWlink/pna/nao.sprd2.gif
./gif2eps.sh nao.sprd2.gif
mv nao.sprd2.eps nao_sprd2.eps

wget -N http://www.cpc.ncep.noaa.gov/products/precip/CWlink/pna/nao.mrf.gif
./gif2eps.sh nao.mrf.gif
mv nao.mrf.eps nao_mrf.eps
