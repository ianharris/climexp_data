#!/bin/sh
dest=$HOME/www2/klimatologie/maandoverzicht_wereldweer/
mo=`date -d "1 month ago" "+%b" | tr "[:upper:]" "[:lower:]"`
yr=`date -d "1 month ago" "+%Y"`
cp -p $yr/*_$mo$yr.png $yr/*_$mo$yr.txt $dest/$yr/
cp -p $yr/*_yr?$yr.png $yr/*_yr?$yr.txt $dest/$yr/
cp -p $yr/*_$mo${yr}_frac.png $yr/*_$mo${yr}_frac.txt $dest/$yr/
cp -p $yr/*_yr?${yr}_frac.png $yr/*_yr?${yr}_frac.txt $dest/$yr/
txtfiles=`ls -t i*.txt | head -50`
cp -p i*.png $txtfiles $dest
cp -p i*yr?.png i*yr?.txt $dest
