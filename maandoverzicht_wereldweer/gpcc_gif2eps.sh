#!/bin/sh
date=$1
if [ -z "$date" ]; then
  echo "usage: $0 monyear"
  exit -1
fi
[ ! -s prcp_gpcc_${date}_eu.gif ] && curl ftp://ftp-anon.dwd.de/pub/data/gpcc/europe_first_guess.gif > prcp_gpcc_${date}_eu.gif
[ ! -s prcp_gpcc_${date}_world.gif ] && curl ftp://ftp-anon.dwd.de/pub/data/gpcc/world_first_guess.gif > prcp_gpcc_${date}_world.gif
for file in prcp_gpcc_???20[01]?_world.gif prcp_gpcc_???20[01]?_eu.gif
do
  f=`basename $file .gif`.eps
  if [ ! -s $f ]; then
    giftopnm $file | pnmcrop | pnmtops > $f
  fi
done
