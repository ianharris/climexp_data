#!/bin/bash

cdo="cdo -r -f nc4 -z zip"
base=https://data.chc.ucsb.edu/products/CHIRPS-2.0/global_daily/netcdf/p25/
version=2.0
for res in 25 # 05
do
    yrnow=`date +%Y`
    yr=1981 
    files=""
    while [ $yr -le $yrnow ]; do
        file=chirps-v$version.$yr.days_p${res}.nc
        if [ ! -s $file -o $yr -ge $((yrnow-1)) ]; then
            echo "downloading $yr"
            wget -N $base$file
        fi # download again?
        files="$files $file"
        ((yr++))
    done # yr
	
	$cdo copy $files v2p0chirps_$res.nc
	file=v2p0chirps_$res.nc
	. $HOME/climexp/add_climexp_url_field.cgi
    $HOME/NINO/copyfiles.sh v2p0chirps_$res.nc
done # res(olution)

# prepend centrends for long dataset in Horn

res=25
cenfile=$HOME/climexp/UCSBData/CenTrends_v1_monthly_ce.nc
if [ ! -s $cenfile ]; then
    (cd $HOME/climexp/UCSBData/; ./update.sh)
fi
if [ ! -s $cenfile ]; then
    echo "$0: error: cannot find CenTrends data:"
    exit -1
fi
$cdo monsum v2p0chirps_$res.nc v2p0chirps_mo_${res}_tmp.nc
$cdo settaxis,1981-01-15,0:00,1month v2p0chirps_mo_${res}_tmp.nc v2p0chirps_mo_${res}.nc
ncatted -a units,pr,m,c,"mm/month" v2p0chirps_mo_$res.nc
$cdo sellonlatbox,28,54,-15,18 v2p0chirps_mo_$res.nc chirps_horn.nc
export HDF5_DISABLE_VERSION_CHECK=1 # no idea why this commend complains and the others do not...
echo "please  ignore the error message"
$cdo remapcon,chirps_horn.nc $cenfile centrends_$res.nc
patchfield centrends_$res.nc chirps_horn.nc none centrends_chirps.nc
# the old version of GrADS on the old server does not plot this properly.
$cdo settaxis,1900-01-15,0:00,1month centrends_chirps.nc tmp.nc; mv tmp.nc centrends_chirps.nc
file=centrends_chirps.nc
. $HOME/climexp/add_climexp_url_field.cgi
$HOME/NINO/copyfiles.sh centrends_chirps.nc
