#!/bin/bash
for ifile in pr_mmday_2daysum_annualmax_spatialavg_*; do
    outfile=${ifile}.dat
    cat > $outfile <<EOF
# contact :: CORDEX precipitation ECSA, rbonnet@ipsl.fr
# precipitation [mm/day] annual maximum of 2-day summed precipitation at ${ifile}
EOF
    cat $ifile >> $outfile
done
