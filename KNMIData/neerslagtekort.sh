#!/bin/bash
# official Dutch average
wget -N https://cdn.knmi.nl/knmi/map/page/klimatologie/gegevens/rdev_tijdreeks.zip
unzip -u -o rdev_tijdreeks.zip
if [ ! -s hist_Ptekort_dagelijks_1apr-30sep_1906-2019_EV24-13_va2001.txt ]; then
    echo "$0: need hist_Ptekort_dagelijks_1apr-30sep_1906-2019_EV24-13_va2001.txt"
    exit -1
fi
cat > nt_nl.dat <<EOF
# precipitation_deficit [mm/dy] cumulative potential evaporatiojn minus precipitation in the Netherlands
# starts 1 April, set to zero when negative.
# institution :: KNMI
# contact :: jules.beersma@knmi.nl
# source :: https://www.knmi.nl/nederland-nu/klimatologie/geografische-overzichten/neerslagtekort_droogte
EOF
cat hist_Ptekort_dagelijks_1apr-30sep_1906-2019_EV24-13_va2001.txt >> nt_nl.dat
cat Ptekort_dagelijks_1apr-30sep_2020.txt >> nt_nl.dat
egrep -v '[a-z]' rdev_tijdreeks.txt >> nt_nl.dat
$HOME/NINO/copyfilesall.sh nt_nl.dat

export PATH=$PATH:$HOME/climexp/bin # just to make sure
# the progs are used to running from ~/climexp...
[ ! -L bin ] && ln -s ~/climexp/bin
[ ! -L KNMIData ] && ln -s . KNMIData
mkdir -p data # references to ensembles are to ./data/$file ...
rm -f data/* # empty cache...
#
#   "neerslagtekort" = potential evaporation according to Makkink minus precipitation
#   only compute averages, not individual stations (bad S/N)
#
for region in all coastal inland; do
    for var in ev rh; do
        case $var in
            ev) long_name="Makkink evaporation $region";;
            rh) long_name="0-24 precipitation $region";;
            *) echo "$0: internal error"; exit -1;;
        esac
        oldlistname=list_${var}.txt
        if [ $region != all ]; then
            listname=${oldlistname%.txt}_${region}.txt
            select_min_years $oldlistname 10 mask $HOME/climexp/countries/Netherlands_$region.txt > $listname
        else
            listname=$oldlistname
        fi
        outfile=${var}_mean_$region
        if [ ! -s $outfile.dat -o $outfile.dat -ot rh260.dat ]; then
            echo "average_ensemble file $listname getdutch$var mean > $outfile.dat"
            average_ensemble file $listname getdutch$var mean > $outfile.dat
        fi
        if [ ! -s $outfile.nc -o $outfile.nc -ot $outfile.dat ]; then
            dat2nc $outfile.dat i "$long_name" $outfile.nc
        fi
    done
    if [ ! -s nt_inst_$region.dat -o nt_inst_$region.dat -ot rh_mean_$region.dat ]; then
        echo "normdiff ev_mean_$region.dat rh_mean_$region.dat f f > nt_inst_$region.dat"
        normdiff ev_mean_$region.dat rh_mean_$region.dat f f > nt_inst_$region.dat
    fi
    if [ ! -s nt_mean_$region.dat -o nt_mean_$region.dat -ot nt_inst_$region.dat ]; then
        echo "cumul nt_inst_$region.dat mon1 4 sel 6 mon2 9 normsd > nt_mean_$region.dat"
        cumul nt_inst_$region.dat mon 4 sel 6 normsd | sed -e 's@/day dy@@' > nt_mean_$region.dat
    fi
done
$HOME/NINO/copyfiles.sh nt_*.dat