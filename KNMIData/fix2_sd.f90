program fix2_sd
!
!   the KNMI snow data database has errors that I try to correct for here.
!
    implicit none
    integer,parameter :: yrbeg=1800,yrend=2050,npermax=366
    integer :: i,j,yr,mo,dy,nperyear,dpm(12)
    real :: data(366,yrbeg:yrend),fixdat
    logical :: lstandardunits,lwrite,lfixit
    character :: file*1024,line*1024
    character units*80,var*80,lvar*120,svar*120,history*50000,metadata(2,100)*1000
    integer :: init=0
    integer,external :: leap
    data dpm /31,29,31,30,31,30,31,31,30,31,30,31/

    lstandardunits = .false.
    lwrite = .false.
    lfixit = .false.
    fixdat = 3e33
    call getarg(1,file)
    if ( file == ' ' ) then
        write(0,*) 'usage: fix2_sd file'
        call exit(-1)
    end if
    call readseriesmeta(trim(file),data,npermax,yrbeg,yrend,nperyear, &
        var,units,lvar,svar,history,metadata,lstandardunits,lwrite)
    do yr=1961,1967
        do mo=7,8
            do dy=10,30,10
                j = 31+29+31+30+31+30 + 31*(mo-7) + dy
                if ( data(j,yr) /= 0 .and. data(j,yr) < 1e33 ) then
                    if ( init == 0 ) then
                        init = 1
                        write(0,*) 'fix2_sd: series ',trim(file),' needs fixing'
                        lfixit = .true.
                    end if
                    if ( fixdat == 3e33 ) then
                        fixdat = data(j,yr)
                    else
                        if ( data(j,yr) /= fixdat ) then
                            write(0,*) 'fix2_sd: hmmm.... different values for July and August snow... Fascinating.'
                            write(0,*) 'fix2_sd: ',data(j,yr),fixdat
                        end if
                    end if
                end if
            end do
        end do
    end do
    if ( lfixit ) then
        do yr=1961,1967
            j = 0
            do mo=1,12
                do dy=1,dpm(mo)
                    j = j + 1
                    if ( dy == 10 .or. dy == 20 .or. dy == 30 ) then
                        if ( data(j,yr) == fixdat ) then
                            data(j,yr) = 3e33
                        end if
                    end if
                end do
            end do
        end do      
        call system('mv '//trim(file)//' '//trim(file)//'.org')
        open(1,file=trim(file))
        call printvar(1,var,units,lvar)
        call printmetadata(1,trim(file),' ','sneeuwdekdikte',history,metadata)
        call printdatfile(1,data,npermax,nperyear,yrbeg,yrend)
    end if
end program fix2_sd
