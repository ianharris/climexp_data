#!/bin/bash
export REMOTE_ADDR=127.0.0.1
iens=0
j=0
for dataset in eobsprivate # racmo # eobs
do
    case $dataset in
        eobs*) nens=1;;
        racmo) nens=16;;
        *) eco "$0: error: unknown dataset $dataset"; exit -1;;
    esac
    iens=0
    while [ $j -lt 4 ]; do
        lat1=`echo "47.25+($j*1.25)" | bc -l`
        lat2=`echo "$lat1+1.25" | bc -l`
        i=0
        while [ $i -lt 4 ]; do
            lon1=`echo "3.25+($i*2)" | bc -l`
            lon2=`echo "$lon1+2" | bc -l`
            if [ $i = 3 -a \( $j = 0 -o $j = 1 \) ]; then
                echo "skipping box 4 and 8"
            else
                iiens=0
                while [ $iiens -lt $nens ]; do
                    ens=`printf %03i $iens`
                    ensii=`printf %02i $iiens`
                    set -x
                    series=prcp_${dataset}_pool_$ens.dat
                    if [ $dataset = eobs ]; then
                        eobs=$HOME/climexp/ENSEMBLES/rr_0.25deg_reg_v23.1ee.nc
                    elif [ $dataset = eobsprivate ]; then
                        eobs=$HOME/climexp/ENSEMBLES/rr_0.25deg_ensmean_private.nc
                    fi
                    racmo=$HOME/climexp/KNMI14Data/CMIP5/output/KNMI/RACMO22E/rcp85/day/atmos/pr/pr_WEU-11i_KNMI-EC-EARTH_historical-rcp85_KNMI-RACMO22E_v1_day_1950-2100_$ensii.nc
                    if [ $dataset = eobs -o $dataset = eobsprivate ]; then
                        if [ ! -s $series -o $series -ot $eobs ]; then
                            get_index $eobs $lon1 $lon2 $lat1 $lat2 > $series
                        fi
                        if [ ! -s $series -o $series -ot $eobs ]; then
                            get_index $eobs $lon1 $lon2 $lat1 $lat2 > $series
                        fi
                    elif [ $dataset = racmo ]; then
                        if [ ! -s $series  ]; then
                            get_index $racmo $lon1 $lon2 $lat1 $lat2 standardunits > $series
                        fi
                        if [ ! -s $series ]; then
                            get_index $racmo $lon1 $lon2 $lat1 $lat2 standardunits > $series
                        fi                    
                    fi
                    ncfile=${series%dat}nc
                    if [ ! -s $ncfile -o $ncfile -ot $series ]; then
                        dat2nc $series p ${series%.dat} $ncfile
                    fi
                    for ave in 1 2; do
                        maxseries=rx${ave}day_${dataset}_pool_$ens.dat
                        if [ ! -s $maxseries -o $maxseries -ot $series ]; then
                            daily2longer $series 2 max ave $ave > tmp$series
                            fgrep '#' tmp$series > $maxseries
                            echo '# selected Apr-Sep season' >> $maxseries
                            fgrep -v '#' tmp$series | awk '{print $1 " " $3}' >> $maxseries
                            rm tmp$series
                        fi
                    done
                    if [ $dataset = eobs ]; then
                        logfile=fit_${series%.dat}.log
                        gmst=$HOME/climexp/NASAData/giss_al_gl_a_4yrlo.dat
                        if [ ! -s $logfile -o $logfile -ot $series ]; then
                            attribute_amoeba $series $gmst GEV assume scale mon 4 sel 6 restrain 0.4 cov1 -1.2 end2 2021 > $logfile
                        fi
                    fi
                    set +x    
                    ((iens++))
                    ((iiens++))
                done
            fi
            ((i++))
        done
        ((j++))
    done

    if [ $dataset = eobs ]; then
        fgrep '&sigma;/&mu;' fit_*.log | awk '{print $4 " " $5  " " $6}' | sed -e 's/<[^>]*>//g' -e 's/\.\.\.//' > pool_disp.txt
        echo "# dispersion parameters of Roberts 14 regions" > pool_disp_1.txt
        cat pool_disp.txt | sed -e 's/^/1900 2021 /' -e 's/$/ 95 pool/' >> pool_disp_1.txt

        fgrep '&xi;:' fit_*.log | awk '{print $4 " " $5  " " $6}' | sed -e 's/<[^>]*>//g' -e 's/\.\.\.//' > pool_shape.txt
        echo "# shape parameters of Roberts 14 regions" > pool_shape_1.txt
        cat pool_shape.txt | sed -e 's/^/1900 2021 /' -e 's/$/ 95 pool/' >> pool_shape_1.txt
    fi
done