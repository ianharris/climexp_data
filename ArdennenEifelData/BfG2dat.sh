#!/bin/bash
for ifile in 1 2; do
    case $ifile in
        1) file=Altenahr_AHR_QDayMean.csv;outfile=discharge_bfg_ahr.dat;metadata="http://213.139.159.46/prj-wwvauskunft/projects/messstellen/wasserstand/register1.jsp?intern=false&msn=2718040300&dfue=1";;
        2) file=Neubrück_ERFT_QDayMean.csv;outfile=discharge_bfg_erft.dat;metadata="http://luadb.lds.nrw.de/LUA/hygon/pegel.php?stationsinfo=ja&stationsname=Neubrueck&ersterAufruf=aktuelle%2BWerte"
    esac
    cat > $outfile <<EOF
# discharge [m3/s] daily mean discharge at ${file%%_*}
# institution :: BfG
# contact :: Nilson@bafg.de
# metadata :: $metadata
EOF
    fgrep -v U $file | fgrep -v 'NA' | fgrep -v 7e- | tr -d '-' | tr ";" " " >> $outfile
done
