#!/bin/bash
for var in t2m # tasmax tasmin
do
    dir=CMIP5/output/KNMI/RACMO22E/rcp85/day/atmos/$var
    for file in $dir/${var}_WEU-11i_KNMI-EC-EARTH_historical-rcp85_KNMI-RACMO22E_v1_day_1950-2100_??.nc
    do
        eval `getunits $file`
        if [ $NX -gt 264 ]; then
            outfile=/tmp/`basename $file`
            set -x
            cdo -r -f nc4 -z zip sellonlatbox,-12.,21.,43.,63. $file $outfile
            set +x
            mv $outfile $file
        fi
    done
done