#!/bin/sh
# move wind speed data from wrong to right place
r=0
while [ $r -lt 16 ]; do
  ((r++))
  # monthly data
  mkdir -p CMIP5/output/KNMI/ECEARTH23/rcp85/mon/atmos/Amon/r${r}i1p1/v1/sfcWind
  mv /data/storage/climexp/ECEARTH23/rcp85/mon/atmos/Amon/r${r}i1p1/v1/sfcWind/*.nc CMIP5/output/KNMI/ECEARTH23/rcp85/mon/atmos/Amon/r${r}i1p1/v1/sfcWind/
  # daily data
  mkdir -p CMIP5/output/KNMI/ECEARTH23/rcp85/day/atmos/day/r${r}i1p1/v1/sfcWind
  mv /data/storage/climexp/ECEARTH23/rcp85/day/atmos/day/r${r}i1p1/v1/sfcWind/*.nc CMIP5/output/KNMI/ECEARTH23/rcp85/day/atmos/day/r${r}i1p1/v1/sfcWind/
  mkdir -p CMIP5/output/KNMI/ECEARTH23/rcp85/day/atmos/day/r${r}i1p1/v1/sfcWindmax
  mv /data/storage/climexp/ECEARTH23/rcp85/day/atmos/day/r${r}i1p1/v1/sfcWindmax/*.nc CMIP5/output/KNMI/ECEARTH23/rcp85/day/atmos/day/r${r}i1p1/v1/sfcWindmax/
done
