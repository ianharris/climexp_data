#!/bin/sh
ifile="$1"
lon1="$2"
lon2="$3"
lat1="$4"
lat2="$5"
ofile="$6"
if [ -z "$ofile" ]; then
    echo "usage: $0 ifile lon1 lon2 la1 lat2 ofile"
    exit -1
fi
if [ ! -s "$ifile" ]; then
    echo "$0: error: cannot locate $ifile"
    exit -1
fi
cdo sellonlatbox,$lon1,$lon2,$lat1,$lat1 $ifile aap$$.nc
cdo fldmean aap$$.nc $ofile
rm aap$$.nc
