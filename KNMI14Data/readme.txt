Data extracted from EC-Earth2.3 T159 and Racmo2.2 12km for location De Bilt. Specifically:

- iknmi14* files: Tas, Tasmin, Tasmax, or precipitation from EC-Earth (tas,
tasmin, pr) or RACMO (tasmax, tasmin), extracted for De Bilt (52N;5.2E),
calculated via the interface (no update script)

- tasmax_Aday_ECEARTH23_rcp85_5E_52N_n_su* and tasmax_Aday_ECEARTH23_rcp85_5.2E_52N_n_su:
tasmax extracted from EC-Earth data (no update script) for De Bilt
(52N;5.2E) and 52N;5E. Note that 52N;5E is just west of De Bilt

- tas_day_ECEARTH23_rcp85_r*i1p1_18600101-21001231_52N_5E.nc files (and tas_day_ECEARTH23_rcp85_r*i1p1_*_52N_5E.nc):
tas EC-Earth, used tdebilt.sh to extract 52N;5E data for EC-Earth and used
cat_series.sh to cat the timeseries. Note that 52N;5E is just west of De Bilt

- t2m_racmo* files: t2m of Racmo (no update script)

- tas_muladdcorr* tasmin_muladdcorr* and tasmax_muladcorr*: EC-Earth at De
Bilt (52N;5.2E) bias corrected: corrected 30-day running means and 30-day
running standard deviation.  More specifically: T_corr = S*T_raw + M, with
M = bias in mean and S = ratio standard deviations, so
T_corr = sig_obs/sig_mod*T_mod - (sig_obs/sig_mod*mu_mod) + mu_obs
no update script, data from Sjoukje Philip sjoukje.philip@knmi.nl

- tasave_muladdcorr*: constructed with make_tas_biascorrected.sh which simply
averages tasmin_muladdcorr* and tasmax_muladcorr*.

- *1-10nov* and *11-20nov*: constructed from
tas_day_ECEARTH23_rcp85_*_18600101-21001231_52N_5E.dat in this folder with
get_decade.sh
