#!/bin/sh
for file in tas_day_ECEARTH23_rcp85_??_18600101-21001231_52N_5E.dat
do
    daily2longer $file 36 mean > aap.dat
    echo "# Tg [Celsius] Tg at 52N,5E 1-10nov EC-Earth2.3 RCP8.5" > ${file%.dat}_1-10nov.dat
    cat aap.dat | awk '{print $1 " " $32}' | fgrep -v '#' >> ${file%.dat}_1-10nov.dat
    echo "# Tg [Celsius] Tg at 52N,5E 11-20nov EC-Earth2.3 RCP8.5" > ${file%.dat}_11-20nov.dat
    cat aap.dat | awk '{print $1 " " $33}' | fgrep -v '#' >> ${file%.dat}_11-20nov.dat
    rm aap.dat
done
