#!/bin/sh

for file in $HOME/climexp/KNMI14Data/CMIP5/output/KNMI/ECEARTH23/rcp85/day/atmos/day/*/v1/tas/*.nc
do
    datfile=`basename $file .nc`_52N_5E.dat
    get_index $file 5 5 52 52 standardunits > $datfile
done
