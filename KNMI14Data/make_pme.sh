#!/bin/sh
cd CMIP5/output/KNMI/ECEARTH23/rcp85/mon/atmos/Amon/pme
r=0
while [ $r -lt 16 ]; do
    r=$((r+1))
    pfile=../r${r}i1p1/v1/pr/pr_Amon_ECEARTH23_rcp85_r${r}i1p1_186001-210012.nc
    [ ! -s $pfile ] && echo "Cannot find file $file" && exit -1
    efile=../r${r}i1p1/v1/evspsbl/evspsbl_Amon_ECEARTH23_rcp85_r${r}i1p1_186001-210012.nc
    [ ! -s $efile ] && echo "Cannot find file $file" && exit -1
    ofile=pme_Amon_ECEARTH23_rcp85_r${r}i1p1_186001-210012.nc
    echo cdo -r -f nc4 -z zip sub $pfile $efile $ofile
    cdo -r -f nc4 -z zip sub $pfile $efile $ofile
done