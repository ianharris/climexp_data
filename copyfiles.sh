#!/bin/sh
files="$@"
if [ -z "$files" ]; then
  echo "usage: $0 files"
  echo "       copies files to Climate Explorer"
fi
cwd=`pwd`
dir=`basename $cwd`
[ -z "$HOST" ] && HOST=`hostname`
if [ $HOST = pc200270.knmi.nl ]; then
  rsync -at "$@" climexp.knmi.nl:climexp/$dir/
  rsync -at "$@" climexp-test.duckdns.org:climexp/$dir/
fi
