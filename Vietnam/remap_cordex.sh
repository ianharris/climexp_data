#!/bin/bash
for dir in `ls -d Vietnam/*/* | fgrep -v _latlon`; do
    dirll=${dir}_latlon
    mkdir -p $dirll
    for file in $dir/*.nc; do
        f=$dirll/`basename $file`
        f=${f%.nc}_latlon.nc
        if [ ! -s $f ]; then
            echo "cdo remapbil,sea_lonlat.txt $file $f"
            cdo remapbil,sea_lonlat.txt $file $f
        fi
    done
    mkdir -p day/pr
    firstfile=`ls $dirll | fgrep RCP | head -1`
    outfile=`echo $firstfile | sed -e 's/RCP\(..\)....../HISTRCP\\1_/'`
    if [ ! -s day/pr/$outfile ]; then
        echo "cdo -r copy $dirll/*.nc day/pr/$outfile"
        cdo -r copy $dirll/*.nc day/pr/$outfile
    fi
    pwd=`pwd`
    cd day/pr/
    i=0
    rm -f pr_EAS-22_HISTRCP85_cordex_??.nc
    for file in *.nc ; do
        if [ -s $file ]; then
            ens=`printf %02i $i`
            lfile=pr_EAS-22_HISTRCP85_cordex_$ens.nc
            ln -s $file $lfile
            ((i++))
        fi
    done
    cd $pwd
done
