India Pakistan heat 2022

This directory contains the observational and model data timeseries used for the WWA study "India Pakistan heat 2022" and the study region mask.

Observational data
------------------
CPC data are provided by Mariam Zachariah at Grantham Institute, Imperial College London, UK (m.zachariah@imperial.ac.uk)
IMD data are provided by Roshan Jha at IDP in Climate Studies, Indian Institute of Technology Bombay, India (zharoshan94@gmail.com)


Model data
----------
HAPPI simulations are provided by Mariam Zachariah at Grantham Institute, Imperial College London, UK (m.zachariah@imperial.ac.uk)

CORDEX CORE is provided by Robert Vautard at Institut Pierre-Simon Laplace, Sorbonne Université, France (robert.vautard@lsce.ipsl.fr)

IPSL-CM6A-LR is provided by Robert Vautard at Institut Pierre-Simon Laplace, Sorbonne Université, France (robert.vautard@lsce.ipsl.fr)

FLOR is provided by Wenchang Yang at Department of Geosciences, Princeton University, USA (wenchang@princeton.edu)

HighResMIP simulations are provided by Sihan Li at School of Geography and the Environment, University of Oxford, UK (sihan.li@ouce.ox.ac.uk)

CMIP6 is provided by Manish Dhasmana at IDP in Climate Studies, Indian Institute of Technology Bombay, India (manishdhasmana49@gmail.com)


Study mask
----------
The study mask is provided by Fahad Saeed at Climate Analytics, Berlin, Germany and Weather and Climate Services, Islamabad, Pakistan (fahad.saeed@climateanalytics.org)
