for infile in ????.txt; do
    outfile=${infile%txt}dat
    # it is late and I do not remember the full regexp syntax. This works.
    sed -e 's/-999.99/-999.9/' -e 's/\([0-9][0-9]\)\([0-9][0-9]\)\([0-9][0-9][0-9][0-9]\)\(.*$\)/\3\2\1\4/' $infile > $outfile
done
