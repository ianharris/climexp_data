#!/bin/bash
force=false
[ "$1" = force ] && force=true
#
#   HadCRUT5
#
base=https://www.metoffice.gov.uk/hadobs/hadcrut5/data/current/analysis/diagnostics/
version=5.0.1.0
for region in global northern_hemisphere southern_hemisphere
do
    file=HadCRUT.${version}.analysis.summary_series.$region.monthly.nc
    cp $file $file.old
    echo "wget -q -N $base/$file"
    wget -q -N $base/$file
    cmp $file $file.old
    if [ $? != 0 -o "$force" = true ]; then
        f=hadcrut5_$region.nc
        ncks -O -v tas_mean $file $f
        $HOME/NINO/copyfilesall.sh $f
    fi
    
    file=HadCRUT.${version}.analysis.ensemble_series.$region.monthly.nc
    cp $file $file.old
    echo "wget -q -N $base/$file"
    wget -q -N $base/$file
    cmp $file $file.old
    if [ $? != 0 -o "$force" = true ]; then
        f=hadcrut5_${region}_@@.nc
        ncks -O -v tas $file tmp$f
        ncpdq -O -a time,realization tmp$f $f
        rm tmp$f
        ncrename -d realization,ens -v realization,ens $f # no longer eeded
        $HOME/NINO/copyfiles.sh $f
    fi
done
#
#   CRUTEM5
#
base=https://www.metoffice.gov.uk/hadobs/crutem5/data/CRUTEM.${version}/diagnostics
for region in global northern_hemisphere southern_hemisphere
do
    file=CRUTEM.${version}.summary_series.$region.monthly.nc
    cp $file $file.old
    echo "wget -q -N $base/$file"
    wget -q -N $base/$file
    cmp $file $file.old
    if [ $? != 0 -o "$force" = true ]; then
        f=crutem5_$region.nc
        ncks -O -v tas $file $f
        $HOME/NINO/copyfilesall.sh $f
    fi
done
#
# HadSST4
#
version=4.0.1.0
base=https://www.metoffice.gov.uk/hadobs/hadsst4/data/csv
for region in GLOBE NHEM SHEM TROP
do
    file=HadSST.${version}_monthly_$region.csv
    cp $file $file.old
    echo "wget -q -N $base/$file"
    wget -q -N $base/$file
    diff $file $file.old
    if [ $? != 0 -o $force = true ]; then
        # only change filename for major updates
        f=HadSST4_monthly_${region}.dat
	    cat > $f <<EOF
# HadSST $version $region averaged SST anomalies, median of ensemble
# source :: <a href="https://www.metoffice.gov.uk/hadobs/hadsst4/data/download.html">Met Office</a>
# SSTa [K] HadSST.$version $region SST anomalies
# institution :: UK Met Office / Hadley Centre
# source_url :: $base/$file
# references :: Kennedy J.J., Rayner, N.A., Smith, R.O., Saunby, M. and Parker, D.E. (2011b). Reassessing biases and other uncertainties in sea-surface temperature observations since 1850 part 1: measurement and sampling errors. J. Geophys. Res., 116, D14103, doi:10.1029/2010JD015218\\n Kennedy J.J., Rayner, N.A., Smith, R.O., Saunby, M. and Parker, D.E. (2011c). Reassessing biases and other uncertainties in sea-surface temperature observations since 1850 part 2: biases and homogenisation. J. Geophys. Res., 116, D14104, doi:10.1029/2010JD015220
# history :: retrieved `date`
# climexp_url :: https://climexp.knmi.nl/getindices.cgi?UKMOData/$f
EOF
        fgrep -v 'year,month' $file | tr '/' ' ' | cut -b 1-15 | tr ',' ' ' >> $f
        $HOME/NINO/copyfiles.sh $f
	fi
done
#
# CETs
#
make cet2dat
make dailycet2dat

cp cetml1659on.dat cetml1659on.dat.old
wget -q -N http://www.metoffice.gov.uk/hadobs/hadcet/cetml1659on.dat
./cet2dat cetml1659on.dat > cet.dat
$HOME/NINO/copyfiles.sh cet.dat

cp cetminmly1878on_urbadj4.dat cetminmly1878on_urbadj4.dat.old
wget -q -N http://www.metoffice.gov.uk/hadobs/hadcet/cetminmly1878on_urbadj4.dat
./cet2dat cetminmly1878on_urbadj4.dat > cet_min.dat
$HOME/NINO/copyfiles.sh cet_min.dat

cp cetmaxmly1878on_urbadj4.dat cetmaxmly1878on_urbadj4.dat.old
wget -q -N http://www.metoffice.gov.uk/hadobs/hadcet/cetmaxmly1878on_urbadj4.dat
./cet2dat cetmaxmly1878on_urbadj4.dat > cet_max.dat
$HOME/NINO/copyfiles.sh cet_max.dat

cp cetdl1772on.dat cetdl1772on.dat.old
wget -q -N http://www.metoffice.gov.uk/hadobs/hadcet/cetdl1772on.dat
./dailycet2dat cetdl1772on.dat > daily_cet.dat
./extend_cet.sh mean
$HOME/NINO/copyfiles.sh daily_cet.dat

cp cetmindly1878on_urbadj4.dat cetmindly1878on_urbadj4.dat.old
wget -q -N http://www.metoffice.gov.uk/hadobs/hadcet/cetmindly1878on_urbadj4.dat
./dailycet2dat cetmindly1878on_urbadj4.dat > daily_cet_min.dat
./extend_cet.sh min
$HOME/NINO/copyfiles.sh daily_cet_min.dat

cp cetmaxdly1878on_urbadj4.dat cetmaxdly1878on_urbadj4.dat.old
wget -q -N http://www.metoffice.gov.uk/hadobs/hadcet/cetmaxdly1878on_urbadj4.dat
./dailycet2dat cetmaxdly1878on_urbadj4.dat > daily_cet_max.dat
./extend_cet.sh max
$HOME/NINO/copyfiles.sh daily_cet_max.dat

# precipitation
make dailyprcp2dat
base=http://www.metoffice.gov.uk/hadobs/hadukp/data
for region in EWP SEEP SWEP CEP NWEP NEEP SP SSP NSP ESP NIP
do
  # daily data
  file=Had${region}_daily_qc.txt
  cp $file $file.old
  wget -q -N $base/daily/$file
  outfile=`basename $file .txt`.dat
  ./dailyprcp2dat $file > $outfile
  $HOME/NINO/copyfiles.sh $outfile

  # monthly data
  file=Had${region}_monthly_qc.txt
  cp $file $file.old
  wget -q -N $base/monthly/$file
  outfile=`basename $file .txt`.dat
  cat > $outfile <<EOF
# From <a href="http://www.metoffice.gov.uk/hadobs/hadukp" target="_new">Hadley Centre</a>
# prcp [mm/month] UKP $region precipitation
# institution :: UK Met Office / Hadley Centre
# source_url :: $base/monthly/$file
# source :: https://www.metoffice.gov.uk/hadobs/hadukp/
# references :: Morice, C. P., J. J. Kennedy, N. A. Rayner, and P. D. Jones (2012), Quantifying uncertainties in global and regional temperature change using an ensemble of observational estimates: The HadCRUT4 dataset, J. Geophys. Res., 117, D08101, doi:10.1029/2011JD017187
# history :: retrieved `date`
# climexp_url :: https://climexp.knmi.nl/getindices.cgi?UKMOData/${outfile%.dat}
EOF
  sed -e 's/-99.9/-999.9/g' -e 's/  0.0/-999.9/g' -e 's/^\([^ ]\)/# \1/' $file >> $outfile
  $HOME/NINO/copyfiles.sh $outfile

done
