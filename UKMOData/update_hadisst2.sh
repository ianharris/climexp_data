#!/bin/bash
wget="wget --no-check-certificate -q -N"
cdo="cdo -r -f nc4 -z zip"
base=https://www.metoffice.gov.uk/hadobs/hadisst2/data
version=2.2.0.0

for var in ice; do # sst not yet ready
    case $var in
        sst) ncvar=sst;;
        ice) ncvar=sic;;
    esac
    file=HadISST.${version}_sea_ice_concentration.nc.gz
    echo "$wget $base/$file"
    $wget $base/$file
    gunzip -c $file > HadISST2_${var}_tmp.nc
    $cdo -selvar,$ncvar -setctomiss,-1000. HadISST2_${var}_tmp.nc HadISST2_$var.nc
    rm HadISST2_${var}_tmp.nc
    file=HadISST2_$var.nc
    . $HOME/climexp/add_climexp_url_field.cgi
    $HOME/NINO/copyfiles.sh HadISST2_$var.nc
done

# enable when SST is available
exit
./makenino.sh
./make_iod.sh
./make_siod.sh
$HOME/NINO/copyfilesall.sh hadisst1_*.dat
