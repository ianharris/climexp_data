#!/bin/bash
#
#   time series
#
make tenday2month
base=https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L4/ice_mass/RL06/v02/mascon_CRI
wget --no-check-certificate -O index_timeseries.html $base
file=`egrep 'antarctica' index_timeseries.html | sed -e 's@^.*>a@a@' -e 's@<.*$@@'` 
wget --no-check-certificate -N "$base/$file"
cat <<EOF > antarctica_mass.dat
# mass [Gt] mass of Antarctica
# doi :: https://doi.org/10.5067/TEMSC-ATS62
# mass of Antarctica from <a href="https://podaac.jpl.nasa.gov/dataset/GREENLAND_MASS_TELLUS_MASCON_CRI_TIME_SERIES_RL06_V2" target="_new">NASA/JPL</a> measured by GRACE
# institution :: NASA/JPL
# references :: Wiese, D. N., D.-N. Yuan, C. Boening, F. W. Landerer, and M. M. Watkins (2016) JPL GRACE Mascon Ocean, Ice, and Hydrology EquivalentHDR Water Height RL05M.1 CRI Filtered Version 2., Ver. 2., PO.DAAC, CA, USA. and Watkins, M. M., D. N. Wiese, D. -N. Yuan, C. Boening, and F. W. Landerer (2015), Improved methods for observing Earth's time variable mass distribution with GRACE using spherical cap mascons, J. Geophys. Res. Solid Earth, 120, 2648_2671, doi: 10.1002/2014JB011547.
# source :: https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L4/ice_mass/RL06/v02/mascon_CRI
# history :: downloaded and converted to climexp conventions on `date`
EOF
./tenday2month $file >> antarctica_mass.dat

file=`egrep 'greenland' index_timeseries.html | sed -e 's@^.*>g@g@' -e 's@<.*$@@'` 
wget --no-check-certificate -N "$base/$file"
cat <<EOF > greenland_mass.dat
# mass [Gt] mass of Greenland
# doi :: https://doi.org/10.5067/TEMSC-GTS62
# mass of Greenland from <a href="https://podaac.jpl.nasa.gov/dataset/GREENLAND_MASS_TELLUS_MASCON_CRI_TIME_SERIES_RL06_V2" target="_new">NASA/JPL</a> measured by GRACE
# institution :: NASA/JPL
# references :: Wiese, D. N., D.-N. Yuan, C. Boening, F. W. Landerer, and M. M. Watkins (2016) JPL GRACE Mascon Ocean, Ice, and Hydrology EquivalentHDR Water Height RL05M.1 CRI Filtered Version 2., Ver. 2., PO.DAAC, CA, USA. and Watkins, M. M., D. N. Wiese, D. -N. Yuan, C. Boening, and F. W. Landerer (2015), Improved methods for observing Earth's time variable mass distribution with GRACE using spherical cap mascons, J. Geophys. Res. Solid Earth, 120, 2648_2671, doi: 10.1002/2014JB011547.
# source :: https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L4/ice_mass/RL06/v02/mascon_CRI
# history :: downloaded and converted to climexp conventions on `date`
EOF
./tenday2month $file >> greenland_mass.dat

base=https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L4/ocean_mass/RL06/v02/mascon_CRI
wget --no-check-certificate -O index_oceanmass.html $base
file=`egrep 'ocean_mass_2' index_oceanmass.html | sed -e 's@^.*>o@o@' -e 's@<.*$@@'`
echo "file=$file"
wget --no-check-certificate -N "$base/$file"
cat <<EOF > ocean_mass.dat
# mass [Gt] mass of ocean
# doi :: https://doi.org/10.5067/TEMSC-OTS62
# mass of ocean from <a href="https://podaac.jpl.nasa.gov/dataset/OCEAN_MASS_TELLUS_MASCON_CRI_TIME_SERIES_RL06_V2" target="_new">NASA/JPL</a> measured by GRACE
# institution :: NASA/JPL
# references :: Wiese, D. N., D.-N. Yuan, C. Boening, F. W. Landerer, and M. M. Watkins (2016) JPL GRACE Mascon Ocean, Ice, and Hydrology EquivalentHDR Water Height RL05M.1 CRI Filtered Version 2., Ver. 2., PO.DAAC, CA, USA. and Watkins, M. M., D. N. Wiese, D. -N. Yuan, C. Boening, and F. W. Landerer (2015), Improved methods for observing Earth's time variable mass distribution with GRACE using spherical cap mascons, J. Geophys. Res. Solid Earth, 120, 2648_2671, doi:10.1002/2014JB011547.
# source :: https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L4/ocean_mass/RL06/v02/mascon_CRI
# history :: downloaded and converted to climexp conventions on `date`
EOF
./tenday2month $file >> ocean_mass.dat

$HOME/NINO/copyfiles.sh *_mass.dat

#
# land
#
# username / password are in ~/.netrc
base=https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L3/grace/land_mass/RL06/v03/CSR
wget --no-check-certificate -N -O grace.html $base
files=`fgrep '.nc<' grace.html | sed -e 's@^.*CSR/@@' -e 's@.nc.*$@.nc@'`
fobase=https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L3/gracefo/land_mass/RL06/v03/CSR
wget --no-check-certificate -N -O gracefo.html $fobase
fofiles=`fgrep '.nc<' gracefo.html | sed -e 's@^.*CSR/@@' -e 's@.nc.*$@.nc@'`
for file in $files $fofiles; do
    if [ ! -s $file ]; then
        c=`echo $file | fgrep -c GRFO`
        if [ $c = 0 ]; then
            wget -N --no-check-certificate $base/$file
        else
            wget -N --no-check-certificate $fobase/$file
        fi
    fi
    line=`describefield $file 2>&1 | fgrep daily`
    year=`echo $line | sed -e 's/^.*from .....//' -e 's/ to .*$//'`
    mon=`echo $line | sed -e 's/^.*from ..//' -e 's/20.. to .*$//'`
    ###echo mon=$mon
    case $mon in
        Jan) m=01;;
        Feb) m=02;;
        Mar) m=03;;
        Apr) m=04;;
        May) m=05;;
        Jun) m=06;;
        Jul) m=07;;
        Aug) m=08;;
        Sep) m=09;;
        Oct) m=10;;
        Nov) m=11;;
        Dec) m=12;;
        *) echo "$0: unknown month $mon"; exit -1;;
    esac
    newfile=grace_land_${year}${m}.nc
    if [ ! -s $newfile ]; then
        cdo settaxis,${year}-${m}-15,0:00,1mon $file $newfile
    fi
done
# concatenate
catnc grace_land_??????.nc grace_land.nc

# mascon land/ocean
wget --no-check-certificate -N https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L3/mascon/RL06/JPL/v02/CRI/netcdf/LAND_MASK.CRI.nc
wget --no-check-certificate -N https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L3/mascon/RL06/JPL/v02/CRI/netcdf/CLM4.SCALE_FACTOR.JPL.MSCNv02CRI.nc
cdo setmisstoc,1. CLM4.SCALE_FACTOR.JPL.MSCNv02CRI.nc scalefactor.nc
base=https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L3/mascon/RL06/JPL/v02/CRI/netcdf
wget --no-check-certificate -O index_all.html $base
file=`egrep 'GRC.*.nc<' index_all.html | sed -e 's@^.*>G@G@' -e 's@<.*$@@'` 
wget --no-check-certificate -N $base/$file
ncks -O -v lwe_thickness $file grace_all_tmp0.nc
cdo settaxis,2002-04-15,0:00,1mon grace_all_tmp0.nc grace_all_tmp1.nc
cdo mul grace_all_tmp1.nc scalefactor.nc grace_all.nc
rm -f grace_all_tmp0.nc grace_all_tmp1.nc

$HOME/NINO/copyfiles.sh grace_land.nc grace_all.nc

exit
# old code
# See mail form Bert Wouters
#   land
base=ftp://podaac.jpl.nasa.gov/allData/tellus/L3/land_mass/RL05/netcdf/
file=\*CSR\*LND\*.nc
wget -q -N "$base/$file"
thisfile=`ls -t $file | head -1`
if [ -s "$thisfile" ]; then
	landfile=$thisfile
fi

#   ocean
base=ftp://podaac.jpl.nasa.gov/allData/tellus/L3/ocean_mass/RL05/netcdf/
file=\*CSR\*OCN\*.nc
wget -q -N "$base/$file"
thisfile=`ls -t $file | head -1`
if [ -s "$thisfile" ]; then
    oceanfile=$thisfile
fi

# this gets rid of the irregular time axis - missing months are set to undefined 
# and non-centered months are assumed centered.
cdo inttime,2003-01-17,0:00,1day $landfile grace_land_daily.nc
daily2longerfield grace_land_daily.nc 12 mean minfac 0.25 grace_land.nc
file=grace_land.nc
. $HOME/climexp/add_climexp_url_field.cgi
$HOME/NINO/copyfiles.sh grace_land.nc
rm grace_land_daily.nc

cdo inttime,2003-01-17,0:00,1day $oceanfile grace_ocean_daily.nc
daily2longerfield grace_ocean_daily.nc 12 mean minfac 0.25 grace_ocean.nc
file=grace_ocean.nc
. $HOME/climexp/add_climexp_url_field.cgi
$HOME/NINO/copyfiles.sh grace_ocean.nc
rm grace_ocean_daily.nc

$HOME/NINO/copyfiles.sh grace_land.nc grace_ocean.nc
