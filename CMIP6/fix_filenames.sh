#!/bin/bash
# the original interpolate.sh script generated the wrong file names: the ripf part was 
# still there next to the Climate Explorer model & ensemble member conventions. Get rid of it.
indirs="$HOME/climexp_data/CMIP6/monthly /data/climexp_cmip6_new/CEeth_192/monthly"

for indir in $indirs; do
    cd $indir
    vars=`ls`
    for var in $vars; do
        cd $var
        pwd
        for file in *.nc; do
            if [ -s $file -a ! -L $file ]; then
                c=`echo $file | grep -c '_r.*i.*p.*f.*_'`
                if [ $c != 0 ]; then
                    newfile=`echo $file | sed -s 's/_r.*i.*p.*f.*_/_/'`
                    mv $file $newfile
                fi # still contains ripf
            fi # regular non-empty file
        done # *.nc
        cd ..
    done # var
done # indir
