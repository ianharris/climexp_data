#!/bin/bash
cdo="cdo -r -f nc4 -z zip"
if [ -d /data/climexp_data ]; then # running on climexp
    indir=/data/climexp_cmip6_new/CEeth
else # running on fileserver
    indir=/data/datamgr/CEeth
fi
cd $indir
vars=`ls`
for var in $vars; do
    case $var in
        pr) interpolate="$cdo remapcon,r192x144";;
        *) interpolate="$cdo remapbil,r192x144";;
    esac
    cd $var
    models=`ls`
    for model in $models; do
        cd $model
        ripfs=`ls`
        for ripf in $ripfs; do
            cd $ripf
            for file in *.nc; do
                if [ -s $file ]; then
                    ### describefield $file
                    scenario=${file#*+}
                    scenario=${scenario%%_*}
                    string=$ripf
                    r=${string#r}
                    r=${r%%i*}
                    string=${string#r$r}
                    i=${string#i}
                    i=${i%%p*}
                    string=${string#i$i}
                    p=${string#p}
                    p=${p%%f*}
                    string=${string#p$p}
                    f=${string#f}
                    if [ ${ripf} != r${r}i${i}p${p}f${f} ]; then
                        echo "$0: error: something went wrong: $ripf != r${r}i${i}p${p}f${f}"
                    fi
                    modelpf=$model
                    if [ $p != 1 ]; then
                        modelpf=${modelpf}-p$p
                    fi
                    if [ $f != 1 -a ${model%f$f} = $model ]; then
                        modelpf=${modelpf}-f$f
                    fi
                    ens=`printf %03i $((r-1+i-1))`
                    outfile=$HOME/climexp_data/CMIP6/monthly/$var/${var}_mon_${modelpf}_${scenario}_${ripf}_192_$ens.nc
                    if [ ! -s $outfile ]; then
                        echo "$interpolate $file $outfile"
                        $interpolate $file $outfile
                    fi
                fi
            done # file
            cd ..
        done # ripf
        cd ..
    done # models
    cd ..
done # vars
