#!/bin/bash
# get rid of the variable file_qf that is present in some files
cd  /data/climexp_cmip6_new/CEeth_192/monthly
vars=`ls`
vars=tas # I know it is only a problem in tas
for var in $vars
do
    cd $var
    for file in *.nc
    do
        if [ -s $file -a ! -L $file ]; then # regular file, not symlink
            c=`ncdump -h $file | fgrep -c file_qf`
            if [ $c != 0 ]; then
                echo ncks -v $var $file tmp$file
                ncks -v $var $file tmp$file
                mv tmp$file $file
            fi # has file_qf
        fi
    done # file
    cd ..
done # vars