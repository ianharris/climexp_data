#! /bin/bash
ABSLINKDIR=/data/climexp_cmip6_new/CEeth_192/monthly/pr/
AVEFILES=pr_*_ave.nc

for linkme in $ABSLINKDIR/$AVEFILES
do
	if [ ! -f ${linkme} ]
	then
		echo "FAIL: ave-file is not a file (${linkme})"
		continue
	fi

	if [ ! -f ${linkme%ave.nc}000.nc ]
	then
		echo "FAIL: 000-file does not exist (${linkme})"
		continue
	else
		for nlinkme in ${linkme%ave.nc}[0-9][0-9][0-9].nc
		do
			if [ ! -f $nlinkme ]
			then
				echo "FAIL: XXX-file is not a file (${nlinkme})"
				continue 2
			fi

			if [ ! -h $(basename $nlinkme) ]
			then
				echo " NEW: " $(basename $nlinkme)
				ln -s $nlinkme $(basename $nlinkme)
			else
				: echo " [OK] " $(basename $nlinkme)
			fi
		done
	fi

done

#ls /data/climexp_cmip6_new/CEeth_192/monthly/pr/pr_mon_one_ssp126_192_[0-9][0-9][0-9].nc
#ls /data/climexp_cmip6_new/CEeth_192/monthly/pr/pr_mon_one_ssp126_192_[0-9][0-9][0-9].nc

