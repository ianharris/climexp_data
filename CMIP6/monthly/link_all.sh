#! /bin/bash

(cd pr; bash link_pr.sh)
(cd psl; bash link_psl.sh)
(cd rsds; bash link_rsds.sh)
(cd tas; bash link_tas.sh)
(cd tasmax; bash link_tasmax.sh)
(cd tasmin; bash link_tasmin.sh)
