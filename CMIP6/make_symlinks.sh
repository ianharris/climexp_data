#!/bin/bash
cdo="cdo -r -f nc4 -z zip"
cd monthly
vars=`ls`
scenarios="ssp126 ssp245 ssp370 ssp585"
for var in $vars
do
    cd $var
    for scenario in $scenarios
    do
        echo $var $scenario
        ione=0
        imod=0
        onefiles=""
        modfiles=""
        onemean=""
        modmean=""
        ensmean=""
        allfiles=`ls *_${scenario}_192_000.nc | egrep -v '_one_|_mod_|_ens_'`
        for file in $allfiles
        do
            file1=${file%000.nc}001.nc
            avefile=${file%000.nc}ave.nc
            files=`echo ${file%000.nc}[0-9][0-9][0-9].nc`
            model=${file#${var}_mon_}
            model=${model%%_*}
            if [ $model = one -o $model = mod -o $model = ens ]; then
                echo "skipping $file"
            else
                ###echo "model=$model"
                ###echo "file1=$file1"
                ###echo "avefile=$avefile"
                ###echo "files=$files"
                if [ ! -s $avefile ]; then
                    if [ -s $file1 ]; then
                        # compute model mean
                        $cdo ensmean $files $avefile
                    else
                        # just one ensemble member, symlink it
                        [ -L $avefile ] && rm $avefile
                        ln -s $file $avefile
                    fi
                fi
                one=`printf %03i $ione`
                onefile=`echo $file | sed -e "s/$model/one/" -e "s/000/$one/"`
                onefiles="$onefiles $onefile"
                [ -z "$onemean" ] && onemean=${onefile%000.nc}ave.nc
                [ -L $onefile ] && rm $onefile
                ln -s $file $onefile
                mod=`printf %03i $imod`
                modfile=`echo $file | sed -e "s/$model/mod/" -e "s/000/$mod/"`
                modfiles="$modfiles $modfile"
                [ -z "$modmean" ] && modmean=${modfile%000.nc}ave.nc
                [ -L $modfile ] && rm $modfile
                ln -s $avefile $modfile
                ((ione++))
                ((imod++))
            fi # valid model
        done # files

        if [ ! -s $onemean ]; then
            echo $cdo ensmean $onefiles $onemean
            $cdo ensmean $onefiles $onemean
            if [ ! -s $onemean ]; then
                echo "$0: something went wrong"; exit -1
            fi
        fi
        if [ ! -s $modmean ]; then
            echo $cdo ensmean $modfiles $modmean
            $cdo ensmean $modfiles $modmean
            if [ ! -s $modmean ]; then
                echo "$0: something went wrong"; exit -1
            fi
        fi

        iens=0
        ensfiles=""
        files=`ls *_${scenario}_192_[0-9][0-9][0-9].nc | egrep -v '_one_|_mod_|_ens_'`
        for file in $files
        do
            ens=`printf %03i $iens`
            model=${file#${var}_mon_}
            model=${model%%_*}
            ensfile=`echo $file | sed -e "s/$model/ens/" -e "s/_[0-9][0-9][0-9]\.nc/_$ens.nc/"`
            [ -z "$ensmean" ] && ensmean=${ensfile%000.nc}ave.nc        
            [ -L $ensfile ] && rm $ensfile
            ln -s $file $ensfile
            ensfiles="$ensfiles $ensfile"
            ((iens++))
        done
        if [ ! -s $ensmean ]; then
            echo $cdo ensmean $ensfiles $ensmean
            $cdo ensmean $ensfiles $ensmean
            if [ ! -s $ensmean ]; then
                echo "$0: something went wrong"; exit -1
            fi
        fi
    done # scenario
    cd ..
done # vars
