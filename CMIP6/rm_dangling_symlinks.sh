#!/bin/bash
indirs="$HOME/climexp_data/CMIP6/monthly /data/climexp_cmip6_new/CEeth_192/monthly"

for indir in $indirs; do
    cd $indir
    vars=`ls`
    for var in $vars; do
        cd $var
        pwd
        for file in *.nc; do
            if [ -L $file ]; then
                c=`echo $file | grep -c '_r.*i.*p.*f.*_'`
                linkname=`ls -l $file | awk '{print $9}'`
                targetfile=`ls -l $file | awk '{print $11}'`
                if [ ! -s $targetfile ]; then
                    echo "cannot find $targetfile"
                    rm $linkname
                fi
            fi # symlink
        done # *.nc
        cd ..
    done # var
done # indir
