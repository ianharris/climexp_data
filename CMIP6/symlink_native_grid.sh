#!/bin/bash
# somehow lost a script like this and eh symlinks have disappeared (my error) so have to redo this.

cd /data/climexp_cmip6_new/CEeth
vars=`ls`
scenarios="ssp126 ssp245 ssp370 ssp585"
for scenario in $scenarios; do
    for var in $vars; do
        cd $var
        models=`ls`
        for model in $models; do
            cd $model
            ripfs=`ls`
            i=0
            ii=`printf %03i $i`
            for ripf in $ripfs; do
                cd $ripf
                pwd
                pwd=`pwd`
                for file in *${scenario}*.nc; do
                    if [ -s $file ]; then
                        (cd ~/climexp_data/CMIP6/monthly/$var; ln -s $pwd/$file ${var}_mon_${model}_${scenario}_${ii}.nc)
                        ((i++))
                        ii=`printf %03i $i`
                    fi
                done
                cd ..
            done
            file=${var}_mon_${model}_${scenario}_ave.nc 
            dir=/data/climexp_cmip6_new/CEeth_192/monthly/$var/
            if [ ! -s $dir/$file ]; then
                # only one ensemble emmber
                (cd ~/climexp_data/CMIP6/monthly/$var; ln -s ${var}_mon_${model}_${scenario}_000.nc ${var}_mon_${model}_${scenario}_ave.nc)
            else
                (cd ~/climexp_data/CMIP6/monthly/$var; ln -s $dir/$file .)
            fi
            cd ..
        done
        cd ..
    done
done