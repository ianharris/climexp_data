#!/bin/bash

set -e

force=true        # set to true to recompute mean/averages
dryrun=1           # set to 0 to execute commands, set to 1 to skip

cdo="cdo -r -f nc4 -z zip"

cd monthly
vars=$(ls -d */)
scenarios="ssp126 ssp245 ssp370 ssp585"

for varslash in $vars
do
    var=${varslash%/}
    cd $var
    for scenario in $scenarios
    do
        echo "=== $var $scenario ===="
        ione=0
        imod=0
        onefiles=""
        modfiles=""
        onemean=${var}_mon_one_${scenario}_192_ave.nc
        modmean=${var}_mon_mod_${scenario}_192_ave.nc
        ensmean=${var}_mon_ens_${scenario}_192_ave.nc

        files=`ls *_${scenario}_192_000.nc | egrep -v '_one_|_mod_|_ens_'`

        # For each model
        for file in $files
        do
            file1=${file%000.nc}001.nc
            avefile=${file%000.nc}ave.nc
            members=`echo ${file%000.nc}[0-9][0-9][0-9].nc`
            model=${file#${var}_mon_}
            model=${model%%_*}

            if [ $model = one -o $model = mod -o $model = ens ]; then
                echo " skipping $file"
            else
                echo " model $model"

                # Model mean
                if [[ ! -s $avefile ]] || $force ; then
                    if [ -s $file1 ]; then
                        echo "   compute $model model mean from $(echo $members | wc -w) members"
                        (( dryrun )) || $cdo ensmean $members $avefile
                    else
                        # just one ensemble member, symlink it
                        echo "   model mean link to its one member only"
                        (( dryrun )) || { [ -L $avefile ] && rm $avefile
                                          ln -s $file $avefile ; }
                    fi
                fi

                # Collect first member of model as (model=one, member=$ione)
                one=`printf %03i $ione`
                onefile=`echo $file | sed -e "s/$model/one/" -e "s/000/$one/"`
                onefiles="$onefiles $onefile"
                echo "   link 1st member to model=one, member $one"
                (( dryrun )) || { [ -L $onefile ] && rm $onefile
                                  ln -s $file $onefile ; }
                ((++ione))

                # Collect model ensemble average as (model=mod, member=$imod)
                mod=`printf %03i $imod`
                modfile=`echo $file | sed -e "s/$model/mod/" -e "s/000/$mod/"`
                modfiles="$modfiles $modfile"
                echo "   link ensemble mean to model=mod, member $mod"
                (( dryrun )) || { [ -L $modfile ] && rm $modfile
                                  ln -s $avefile $modfile ; }
                ((++imod))

            fi # valid model
        done # models

        # Ensemble mean of all 1st members
        if [[ ! -s $onemean ]] || $force
        then
            # Make sure there is no old links dangling around
            for k in $(seq $ione 999); do
                notvalid=${var}_mon_one_${scenario}_192_$(printf %03i $k).nc
                if [[ -h $notvalid ]]
                then
                    (( dryrun )) && echo "DEL: rm -f $notvalid" \
                            || rm -f $notvalid
                fi
            done
            echo " ensemble mean of all 1st members ($(echo $onefiles | wc -w) members)"
            (( dryrun )) || $cdo ensmean $onefiles $onemean
        fi

        # Ensemble mean of all model means
        if [[ ! -s $modmean ]] || $force
        then
            # Make sure there is no old links dangling around
            for k in $(seq $imod 999); do
                notvalid=${var}_mon_mod_${scenario}_192_$(printf %03i $k).nc
                if [[ -h $notvalid ]]
                then
                    (( dryrun )) && echo "DEL: rm -f $notvalid" \
                            || rm -f $notvalid
                fi
            done
            echo " ensemble mean of all model means ($(echo $modfiles | wc -w) members)"
            (( dryrun )) || $cdo ensmean $modfiles $modmean
        fi

        # Ensemble mean of all members across all models
        iens=0
        ensfiles=""
        allfiles=`ls *_${scenario}_192_[0-9][0-9][0-9].nc | egrep -v '_one_|_mod_|_ens_'`

        for file in $allfiles
        do
            ens=`printf %03i $iens`
            model=${file#${var}_mon_}
            model=${model%%_*}
            ensfile=`echo $file | sed -e "s/$model/ens/" -e "s/_[0-9][0-9][0-9]\.nc/_$ens.nc/"`
            (( dryrun )) || { [ -L $ensfile ] && rm $ensfile
                              ln -s $file $ensfile ; }
            ensfiles="$ensfiles $ensfile"
            ((++iens))
        done

        if [[ ! -s $ensmean ]] || $force
        then
            # Make sure there is no old links dangling around
            for k in $(seq $iens 999); do
                notvalid=${var}_mon_ens_${scenario}_192_$(printf %03i $k).nc
                if [[ -h $notvalid ]]
                then
                    (( dryrun )) && echo "DEL: rm -f $notvalid" \
                            || rm -f $notvalid
                fi
            done

            echo " ensemble mean of all members across all models ($(echo $ensfiles | wc -w) members)"
            (( dryrun )) || $cdo ensmean $ensfiles $ensmean
        fi
    done # scenario
    cd ..
done # vars
