#!/bin/bash
# and to fix an error in gteh fix_filenames script...
indirs="$HOME/climexp_data/CMIP6/monthly /data/climexp_cmip6_new/CEeth_192/monthly"

for indir in $indirs; do
    cd $indir
    vars=`ls`
    for var in $vars; do
        cd $var
        pwd
        for file in *.nc; do
            if [ -s $file -a ! -L $file ]; then
                c=`echo $file | grep -c 'ssp..._....nc'`
                if [ $c != 0 ]; then
                    newfile=`echo $file | sed -s 's/_\([0-9][0-9][0-9]\)\.nc/_192_\1.nc/'`
                    mv $file $newfile
                fi # did not contain _192_
            fi # regular non-empty file
        done # *.nc
        cd ..
    done # var
done # indir
