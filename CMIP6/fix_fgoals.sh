#!/bin/bash
for file in *.nc; do
    c=`ncdump -c $file | fgrep -c '60240.5, 60240.5,'`
    if [ $c != 0 ]; then
        echo "$0: $file needs fixing"
        ncks -d time,0,1979 $file deel1.nc
        ncks -d time,1981,2027,2 $file deel2.nc
        ncks -d time,2028,3035 $file deel3.nc
        mv $file $file.old
        ncrcat deel1.nc deel2.nc deel3.nc $file
        rm deel1.nc deel2.nc deel3.nc
    fi
done