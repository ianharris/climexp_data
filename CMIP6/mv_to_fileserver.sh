#!/bin/bash
if [ -d /data/climexp_data ]; then # running on climexp
    indir=/data/climexp_cmip6_new
else # running on fileserver
    echo "$0: please run on climexp"; exit -1
fi
cd $indir/CEeth
vars=`ls`
exps="ssp126 ssp245 ssp370 ssp585"

if [ 0 = 1 ]; then

for var in $vars; do
    mkdir -p $HOME/climexp_data/CMIP6/monthly/$var
    cd $HOME/climexp_data/CMIP6/monthly/$var
    pwd
    models=`(cd $indir/CEeth/$var/; ls)`
    for exp in $exps; do
        for model in $models; do
            for p in 1 2 3; do
                for f in 1 2 3; do
                    iens=0
                    ens=000
                    linkfiles=""
                    for file in $indir/CEeth/$var/$model/r*i*p${p}f${f}/*${exp}_*.nc; do
                        if [ -s $file ]; then
                            modelpf=$model
                            if [ $p -gt 1 ]; then
                                modelpf=${modelpf}-p$p
                            fi
                            if [ $f -gt 1 ]; then
                                modelpf=${modelpf}-f$f
                            fi
                            string=`basename $file`
                            string=${string#${var}_mon_}
                            mmodel=${string%%_*}
                            if [ $model != $model ]; then
                                echo "$0: error: $model $mmodel"
                                exit -1
                            fi
                            string=${string#${model}_}
                            exp=${string%%_*}
                            string=${string#${exp}_}
                            exp=${exp#historical+}
                            ripf=${string%%_*}
                            linkfile=${var}_mon_${modelpf}_${exp}_$ens.nc
                            linkfiles="$linkfiles $linkfile"
                            if [ ! -L $linkfile ]; then
                                ln -s $file $linkfile
                            fi
                            ((iens++))
                            ens=`printf %03i $iens`
                        fi
                    done # files
                    if [ -n "$linkfiles" ]; then
                        avefile=${var}_mon_${modelpf}_${exp}_ave.nc
                        if [ $iens = 1 ]; then
                            ln -s $linkfile $avefile
                        else
                            echo "cdo -r -f nc4 -z zip ensmean $linkfiles $avefile"
                            cdo -r -f nc4 -z zip ensmean $linkfiles $avefile
                        fi
                    fi
                done # f
            done # p
        done # models
    done # exp
done # vars

fi
echo DEBUG

for var in $vars; do
    mkdir -p $HOME/climexp_data/CMIP6/monthly/$var
    cd $HOME/climexp_data/CMIP6/monthly/$var
    pwd
    for file in *_192_*.nc ; do
        if [ -s $file -a ! -L $file ]; then
            newfile=$indir/CEeth_192/monthly/$var/$file
            mv $file $newfile
            ln -s $newfile .
        fi
    done
    for file in  *_ssp???_ave.nc ; do
        if [ -s $file -a ! -L $file ]; then
            newfile=$indir/CEeth_192/monthly/$var/$file # easier
            mv $file $newfile
            ln -s $newfile .
        fi
    done
done