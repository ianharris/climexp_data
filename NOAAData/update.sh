#!/bin/bash

cdo="cdo -r -f nc4 -z zip"
yrnow=`date +%Y`

# PSL RefET (they change names every few years...)

base=ftp://ftp2.psl.noaa.gov/Projects/RefET/global/Gen-0
for resolution in coarse # fine  # I do not have the disk space for fine
do
    base0=$base/${resolution}_resolution
    version=v2
    base0=$base0/data_$version
    if [ ! -s downloaded_$version ]; then
        echo "wget -q -N -r -nH --cut-dirs=4 $base0"
        wget -q -N -r -nH --cut-dirs=4 $base0
        date > downloaded_$version
    else
        echo "wget -q -N -r -nH --cut-dirs=4 $base0/$((yrnow-1))"
        wget -q -N -r -nH --cut-dirs=4 $base0/$((yrnow-1))
        echo "wget -q -N -r -nH --cut-dirs=4 $base0/$yrnow"
        wget -q -N -r -nH --cut-dirs=4 $base0/$yrnow
    fi
    for dir in ${resolution}_resolution/data_$version/????; do
        yr=`basename $dir`
        cwd=`pwd`
        cd ${resolution}_resolution/data_$version
        osfile=ETos_psl_${resolution}_${yr}_dy.nc
        osmofile=ETos_psl_${resolution}_${yr}_mo.nc
        rsfile=ETrs_psl_${resolution}_${yr}_dy.nc
        rsmofile=ETrs_psl_${resolution}_${yr}_mo.nc
        if [ ! -s $osfile -o ! -s $osmofile ]; then
            $cdo copy $yr/ETos*.nc tmp$osfile
            $cdo settaxis,${yr}-01-01,12:00,1day tmp$osfile $osfile
            rm tmp$osfile
            daily2longerfield $osfile 12 mean $osmofile
        fi
        if [ ! -s $rsfile -o ! -s $rsmofile ]; then
            $cdo copy $yr/ETrs*.nc tmp$rsfile
            $cdo settaxis,${yr}-01-01,12:00,1day tmp$rsfile $rsfile
            rm tmp$rsfile
            daily2longerfield $rsfile 12 mean $rsmofile
        fi
        cd $cwd
    done
    for type in os rs; do
        dyfile=ET${type}_psl_${resolution}_dy.nc
        mofile=ET${type}_psl_${resolution}_mo.nc
        $cdo copy ${resolution}_resolution/data_$version/ET${type}_psl_coarse_????_mo.nc $mofile
        export file=$mofile
        . ~/climexp/add_climexp_url_field.cgi
        $HOME/NINO/copyfiles.sh $mofile &
        if [ -n "$use_daily_data" ]; then # not used for the time being
            $cdo copy ${resolution}_resolution/data_$version/ET${type}_psl_coarse_????_dy.nc $dyfile
            export file=$dyfile
            . ~/climexp/add_climexp_url_field.cgi
            $HOME/NINO/copyfiles.sh $dyfile &
        fi
    done
done

# ESRL AGGI

base=https://gml.noaa.gov/aggi
file=AGGI_Table.csv
wget -N --no-check-certificate $base/$file

vars=`head -n 10 $file | fgrep Year`
col=1
while [ $col -lt 10 ]; do
    ((col++))
    total1=true
    var=`echo $vars | cut -d ',' -f $col`
    var=${var%\*}
    echo "var=$var"
    case $var in
        CO2|CH4|N2O|CFC12|CFC11|CFC|HCFCs|HFCs|15-minor|Total)
            if [ $var != Total -o $total1 = true ]; then
                [ $var = Total ] && total1=false
                name="$var radiative forcing"
                units='W/m2'
            elif [ $var = Total ]; then
                var="CO2eq"
                name='equivalent CO2 concentration'
                units="ppm"
            else
                echo "$0: error bcdfhh"; exit -1
            fi;;
        1990*)
            var=AGGI
            name='annual greenhouse gas index'
            units='1'
            ;;
        *) echo "$0: error: unknown var $var";exit -1;;
    esac

    outfile=${var}_noaa.dat
    cat > $outfile <<EOF
# Radiative forcings and annual greenhouse gas index from <a href="https://www.esrl.noaa.gov/gmd/aggi/aggi.html">NOAA</a>
# $var [$units] $name
# institution :: NOAA/ESRL
# source_url :: $base/$file
# source :: https://www.esrl.noaa.gov/gmd/aggi/aggi.html
# contact :: James.H.Butler@noaa.gov
# climexp_url :: https://climexp.knmi.nl/getindices.cgi?NOAAData/$outfile
EOF
    sed -e '1,/Year/d' $file | cut -d ',' -f 1,$col | tr ',' ' ' >> $outfile
done 
$HOME/NINO/copyfilesall.sh *_noaa.dat

# MEI

cp meiv2.data meiv2.data.old
url=https://www.esrl.noaa.gov/psd/enso/mei/data/meiv2.data
wget --no-check-certificate -N $url
cat > meiv2.dat <<EOF
# MEI [1] Multivariate ENSO Index v2
# shifted by 0.5 month, i.e., the Jan value represents the Dec/Jan MEI index.
# from <a href="https://www.esrl.noaa.gov/psd/enso/mei/">ESRL</a>
# insitution :: NOAA/ESRL
# link :: https://www.esrl.noaa.gov/psd/enso/mei/
# source :: $url
# history :: retrieved from NOAA/ESRL on `date`
EOF
tail -n +2 meiv2.data | egrep '^[12][0-9]' | sed -e 's/-999.00/ -999.9/g' >> meiv2.dat
$HOME/NINO/copyfiles.sh meiv2.dat

if [ -n "$MEI_UPDATED_AGAIN" ]; then
cp table.html table.html.old
url=https://www.esrl.noaa.gov/psd/enso/mei/table.html
wget -N $url
cat > mei.dat <<EOF
# MEI [1] Multivariate ENSO Index
# shifted by 0.5 month, i.e., the Jan value represents the Dec/Jan MEI index.
# from <a href="https://www.esrl.noaa.gov/psd/enso/mei/">ESRL</a>
# insitution :: NOAA/ESRL
# author :: Klaus Wolters
# link :: https://www.esrl.noaa.gov/psd/enso/mei/
# source :: $url
# history :: retrieved from NOAA/ESRL on `date`
EOF
egrep '^[12][0-9]' table.html >> mei.dat
lastline=`tail -1 mei.dat`
ndef=`echo $lastline | wc -w`
ndef=$((ndef - 1))
nundef=$((12 - ndef))
undef=""
while [ $nundef -gt 0 ]; do
  undef="$undef -999.9"
  nundef=$((nundef - 1))
done
sed -e "s/$lastline/$lastline $undef/" mei.dat > aap.dat
mv aap.dat mei.dat
$HOME/NINO/copyfiles.sh mei.dat

fi # MEI

# OLR

cp olr.mon.mean.nc olr.mon.mean.nc.old
wget -N ftp://ftp.cdc.noaa.gov/Datasets/interp_OLR/olr.mon.mean.nc
describefield olr.mon.mean.nc
$HOME/NINO/copyfiles.sh olr.mon.mean.nc
