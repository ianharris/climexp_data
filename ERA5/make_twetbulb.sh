#!/bin/bash
# compute wet bulb temperatures
cdo="cdo -r -f nc4 -z zip"
regions="05 eu af na"
for region in $regions; do
    mkdir -p $region/twetbulb
    for tmax_file in $region/tmax/era5_??????_tmax.nc; do
        date=`basename $tmax_file | sed -e 's/^[^_]*_//' -e 's/_.*$//'`
        tdew_file=$region/tdew/era5_${date}_tdew.nc
        sp_file=$region/sp/era5_${date}_sp.nc
        twetbulb_file=$region/twetbulb/era5_${date}_twetbulb.nc
        if [ ! -s $tdew_file ]; then
            echo "$0: error: cannot find $tdew_file"
        elif [ ! -s $sp_file ]; then
            echo "$0: error: cannot find $sp_file"
        elif [ ! -s $twetbulb_file -o $twetbulb_file -ot $tmax_file \
            -o $twetbulb_file -ot $tdew_file -o $twetbulb_file -ot $sp_file ]; then
            ./fix_units.sh $tmax_file
            ./fix_units.sh $tdew_file
            ./fix_units.sh $sp_file            
            wetbulb_field $tmax_file $tdew_file $sp_file $twetbulb_file
            if [ ! -s $twetbulb_file ]; then
                echo "$0: something went wrong computing $twetbulb_file"
                exit -1
            fi
        fi
    done
    var=twetbulb
    lastfile=`ls -t $region/twetbulb/ | head -n 1`
    if [ $region = 05 ]; then
        file=era5_${var}_daily.nc
    else
        file=era5_${var}_daily_$region.nc
    fi
    if [ ! -s $file -o $lastfile -nt $file -o "$1" = force ]; then
        echo $cdo copy $region/twetbulb/era5_??????_twetbulb.nc $file
        $cdo copy $region/twetbulb/era5_??????_twetbulb.nc $file
        ncatted -a title,global,c,c,"ERA5 reanalysis, https://www.ecmwf.int/en/forecasts/datasets/reanalysis-datasets/era5" $file
        . $HOME/climexp/add_climexp_url_field.cgi
        $HOME/NINO/copyfiles.sh $file &
    fi
    dayfile=$file
    file=era5_twetbulb.nc
    if [ ! -s $file -o $file -ot $dayfile ]; then
        daily2longerfield $dayfile 12 mean $file
        . $HOME/climexp/add_climexp_url_field.cgi
        $HOME/NINO/copyfiles.sh $file &        
    fi
done
