#!/bin/bash
# fixes the wrong units Philippe gives me
file="$1"
if [ -z "$file" ]; then
    echo "usage: $0 file"
    exit -1
fi
eval `getunits $file`
case "$UNITS" in
    "KK") UNITS="K";;
    "PaPa") UNITS="Pa";;
    "kg kg**-1kg/kg") UNITS="kg/kg";;
    "m of water equivalentmm/dy") UNITS="mm/dy";;
    "mmm/dy") UNITS="mm/dy";;
    "m**2 s**-2m2 s-2") UNITS="m2 s-2";;
    *) UNITS="";;
esac
[ "$VAR" = evap ] && UNITS="mm/dy"
if [ -n "$UNITS" ]; then
    ncatted -a units,$VAR,m,c,"$UNITS" $file
fi
